---
layout: handbook-page-toc
title: "CS Top 10 List for Product"
---

# CS Top 10 List for Product
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

## Deprecated

The CS Top 10 list for Product has been deprecated. It has been [replaced with a more quantitative method using Periscope](/handbook/customer-success/tam/product/). Please follow the process prescibed on that page to indicate customer interest in issues to Product Management.
