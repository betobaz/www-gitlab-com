---
layout: markdown_page
title: "TMRG - GitLab DiversABILITY"
description: "Helpful resources, support, and activities for differently abled team members or who are caring for a differently abled loved one."
canonical_path: "/company/culture/inclusion/erg-gitlab-diversability/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
We will start planning a launch event for the DiversABILITY TMRG in July 2020.  We look forward to providing helpful resources, support, and activities for our team members who are differently abled or who are caring for a differently abled loved one.


## Mission 

To provide everyone a safe, inclusive, respectful environment where they can use their unique gifts to contribute, lead, and prosper.


## Leads
* [Melody Maradiaga](https://about.gitlab.com/company/team/#mmaradiaga) 


## Executive Sponsor
* [Dave Gilbert](https://about.gitlab.com/company/team/#davegilbert) - VP of Recruiting


## Upcoming Events




## Additional Resources
